from vboxharden.cli.vboxharden import cmd


def main():
    import argparse

    argp = argparse.ArgumentParser('vboxharden')

    subp = argp.add_subparsers()

    cmd._register_all(subp)

    args = argp.parse_args()
    if hasattr(args,'cmd'):
        args.cmd(args)
    else:
        argp.print_usage()

if __name__ == '__main__':
    main()
import argparse
from vboxharden.cli.vboxharden.cmd.configure import _register as _register_configure
from vboxharden.cli.vboxharden.cmd.build import _register as _register_build
from vboxharden.cli.vboxharden.cmd.clean import _register as _register_clean
from vboxharden.cli.vboxharden.cmd.install import _register as _register_install

def _register_all(subp: argparse._SubParsersAction) -> None:
    _register_build(subp)
    _register_clean(subp)
    _register_configure(subp)
    _register_install(subp)
import argparse
import datetime
from pathlib import Path
import shutil
import os

from buildtools import os_utils
from vboxharden.logging import ClickWrap

from vboxharden.utils import existingDirPath
from vboxharden.config import Config

import click_spinner

log = ClickWrap()

def _register(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('build')
    p.add_argument('--clobber', '-C', action='store_true', default=False)
    p.add_argument('--stage', '-s', type=int, default=1)
    p.add_argument('--jobs', '-j', type=int, default=None, help='Passed to kmk')
    p.set_defaults(cmd=cmd_build)

def _patchBIOSFile(file: Path, newDate: datetime.date) -> bool:
    # 0xea, 0x5b, 0xe0, 0x00, 0xf0, 0x30, 0x36, 0x2f, 0x32, 0x33, 0x2f, 0x39, 0x39, 0x00, 0xfc, 0x9d, /* 0x0000fff0: .[...06.23.99... */
    datestr = newDate.strftime('%m.%d.%y')
    datebytestr = ', '.join(hex(ord(c)) for c in datestr)
    try:
        contents = file.read_text()
        contents = contents.replace('06.23.99', datestr)
        contents = contents.replace('0x30, 0x36, 0x2f, 0x32, 0x33, 0x2f, 0x39, 0x39', datebytestr)
        file.write_text(contents)
    except Exception as e:
        with log.error(f'Failed to patch {file}:'):
            log.error(str(e))
            return False
    log.success(f'Successfully patched {file}!')
    return True

def _patchKMKMD5Sum(KMK_DIR: Path, MD5SUMOUT: Path) -> bool:
    subject: Path = KMK_DIR / 'kmk_md5sum'
    with log.info(f'Patching {subject}...'):
        with subject.open('w') as f:
            f.write(f'''#!/bin/bash
echo $2 >>{MD5SUMOUT}
echo $2
echo
''')
        # BUG: Removing this try/catch results in weird early return shit. (Python 3.8.10 on Ubuntu)
        try:
            subject.chmod(0o755)
        except Exception as e:
            log.error(str(e))
            print(repr(e))
            return False
        stat = subject.stat()
        expected = 36+len(str(MD5SUMOUT))
        assert stat.st_size == expected, f'{stat.st_size} != {expected}'
        assert stat.st_mode == 0o100755, f'{stat.st_mode} != 0o100755'
        log.success(f'Done! Sz={stat.st_size}, Mode={oct(stat.st_mode)}')
    return True

def _patchBIOSFiles(PC_BIOS_BIN: Path, newDate: datetime.date) -> bool:
    with log.info("Patching BIOS date in automatically generated files..."):
        for partnum in ['286', '386', '8086']:
            _patchBIOSFile(PC_BIOS_BIN / f'PcBiosBin{partnum}.c', newDate)

def cmd_build(args: argparse.Namespace) -> None:
    config = Config()
    config.load(Config.FILENAME)
    if args.clobber:
        if config.paths.TMPDIR.is_dir():
            log.info(f'Clearing old builds at {config.paths.TMPDIR}.')
            with click_spinner.spinner(force=True):
                shutil.rmtree(config.paths.TMPDIR)
        config.paths.reconfigureTmp()
        config.save(Config.FILENAME)
    log.info(f'TMPDIR = {config.paths.TMPDIR}')

    stage = args.stage

    kmkargs = []
    makeargs = []
    if args.jobs is not None:
        kmkargs.append(f'--jobs={args.jobs}')
        makeargs += ['-j', str(args.jobs)]
    kmkargs = ' '+(' '.join(kmkargs))
    makeargs = ' '+(' '.join(makeargs))

    # Stage 1: /orig
    if stage == 1 and not config.paths.ORIG_DIR.is_dir():
        log.info(f'Copying {config.paths.SOURCESDIR} -> {config.paths.ORIG_DIR}')
        with click_spinner.spinner(force=True):
            #config.paths.ORIG_DIR.mkdir(parents=True, exist_ok=True)
            shutil.copytree(config.paths.SOURCESDIR, config.paths.ORIG_DIR)
        stage = 2

    if stage == 2:
        with os_utils.Chdir(str(config.paths.ORIG_DIR)):
            configure = config.paths.ORIG_DIR / 'configure'
            configure.chmod(mode=0o775)
            os_utils.cmd([str(configure), '--disable-hardening'], echo=True, show_output=True)
            COMPILE_SH = config.paths.ORIG_DIR / 'COMPILE.sh'
            with COMPILE_SH.open('w') as f:
                LINUX_MODULE_PATH = config.paths.ORIG_DIR / 'out' / 'linux.amd64' / 'release' / 'bin' / 'src'
                f.write(f'''#!/bin/bash
cd {config.paths.ORIG_DIR}
source {config.paths.ORIG_DIR}/env.sh
kmk{kmkargs}
cd {LINUX_MODULE_PATH}
make

cd {config.paths.ORIG_DIR}
source {config.paths.ORIG_DIR}/env.sh
kmk clean
''')
            os_utils.cmd(['bash', 'COMPILE.sh'], echo=True, show_output=True)
            stage = 3
            
    if stage == 3:
        if not config.strings.patch():
            config.save(Config.FILENAME)
            return
        config.save(Config.FILENAME)
        stage = 4
    
    if stage == 4:
        with os_utils.Chdir(str(config.paths.PATCHED_DIR)):
            configure = config.paths.PATCHED_DIR / 'configure'
            configure.chmod(mode=0o775)
            os_utils.cmd([str(configure), '--disable-hardening'], echo=True, show_output=True)
            _patchKMKMD5Sum(config.paths.KMKTOOLSSUBDIR, config.paths.MD5SUMOUT)
            COMPILE_SH = config.paths.PATCHED_DIR / 'COMPILE.sh'
            with COMPILE_SH.open('w') as f:
                f.write(f'''#!/bin/bash
cd {config.paths.PATCHED_DIR}
source {config.paths.PATCHED_DIR}/env.sh
kmk{kmkargs}
''')
            if not os_utils.cmd(['bash', 'COMPILE.sh'], echo=True, show_output=True):
                log.error('Compile failed! See log for details.')
                return False
            _patchBIOSFiles(
                config.paths.PATCHED_DIR / 'out' / 'linux.amd64' / 'release' / 'obj' / 'PcBiosBin', 
                config.strings.biosDate)
            log.info('Compiling again...')
            with COMPILE_SH.open('w') as f:
                LINUX_MODULE_PATH = config.paths.PATCHED_DIR / 'out' / 'linux.amd64' / 'release' / 'bin' / 'src'
                f.write(f'''#!/bin/bash
cd {config.paths.PATCHED_DIR}
source {config.paths.PATCHED_DIR}/env.sh
kmk{kmkargs}

cd {LINUX_MODULE_PATH}
make{makeargs}
''')
            if not os_utils.cmd(['bash', 'COMPILE.sh'], echo=True, show_output=True):
                log.error('Compile failed! See log for details.')
                return False
    with log.success('All compiling is complete!'):
        log.raw('To install: vboxharden install')
        log.raw('To uninstall: vboxharden uninstall')
    config.save(Config.FILENAME)
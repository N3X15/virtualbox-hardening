import argparse
from pathlib import Path
import shutil
import os

from buildtools import os_utils
from vboxharden.logging import ClickWrap

from vboxharden.utils import existingDirPath
from vboxharden.config import Config

import click_spinner

log = ClickWrap()

def _register(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('clean')
    p.set_defaults(cmd=cmd_clean)

def cmd_clean(args: argparse.Namespace) -> None:
    config = Config()
    config.load(Config.FILENAME)
    with log.info('Clearing old builds...'):
        for dir in Path('/tmp').glob('vboxharden-*'):
            with click_spinner.spinner(force=True):
                shutil.rmtree(dir)
            log.success(f'Removed {dir}')
    
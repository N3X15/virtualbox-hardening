import argparse

from vboxharden.utils import existingDirPath
from vboxharden.config import Config

import click

def _register(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('configure', aliases=['config'])
    p.add_argument('source_dir', type=existingDirPath, help='Path to virtualbox source code directory')
    p.set_defaults(cmd=cmd_configure)

def cmd_configure(args: argparse.Namespace) -> None:
    config = Config()
    if Config.FILENAME.is_file():
        config.load(Config.FILENAME)
    config.configure(args.source_dir)
    config.dump()
    config.save(Config.FILENAME)
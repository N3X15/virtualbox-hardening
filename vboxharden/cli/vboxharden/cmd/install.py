import argparse
import os
import click_spinner
from ruamel.yaml import YAML
from pathlib import Path
from typing import List, Set

import tqdm
from vboxharden.config import Config
from vboxharden.logging import ClickWrap
from buildtools import os_utils

log = ClickWrap()
yaml = YAML(typ='rt')

KMODS_INSTALLED_FILE = Path('installed_kmods.yml')

def _register(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser('install')
    p.add_argument('--jobs', '-j', type=int, default=None, help='Number of threads to spin off (usually the number of processor cores present)')
    p.set_defaults(cmd=cmd_install)


def symlink(from_: Path, to: Path) -> bool:
    if to.is_symlink():
        log.success(f'{from_} -> {to} (exists)')
        return True
    if os_utils.cmd(['ln', '-s', str(from_), str(to)], show_output=True):
        log.success(f'{from_} -> {to}')
        return True
    else:
        log.error(f'{from_} -> {to}')
        return False
def remove_kmods() -> None:
    allmods: Set[str] = set()
    rmmods: Set[str] = set()

    triedToRemoveShit = False
    while triedToRemoveShit:
        output = os_utils.cmd_out(['lsmod'])
        for line in output.splitlines(False):
            cells = line.split(' ')
            allmods = cells[0]

        rmmods.update(filter(lambda x: x.startswith('vbox'), allmods))
        if KMODS_INSTALLED_FILE.is_file():
            with KMODS_INSTALLED_FILE.open('r') as f:
                data = yaml.load(f)
                rmmods.update(filter(lambda m: m in data['installed-kernel-modules'], allmods))
        
        if len(rmmods) > 0:
            with log.info(f'Removing {len(rmmods)} kernel modules...'):
                for mod in rmmods:
                    triedToRemoveShit = True
                    if not os_utils.cmd(['sudo', 'rmmod', mod], echo=False, show_output=True):
                        log.error(f'Failed to sudo rmmod {mod}')
                    else:
                        log.success(f'sudo rmmod {mod}')

def cmd_install(args: argparse.Namespace) -> None:
    SYSTEM_MAP_LOC = Path('/boot') / f'System.map-{os.uname().release}'
    if SYSTEM_MAP_LOC.is_file():
        log.success(f'Selected {SYSTEM_MAP_LOC}')
    else:
        log.error('Could not find System.map!')
        return
    remove_kmods()
    installed_ko: List[str] = []
    config = Config()
    config.load(Config.FILENAME)
    def install_kernel_module(modname: str) -> None:
        if os_utils.cmd(['sudo', 'modprobe', modname], echo=False, show_output=True):
            log.success(f'Success: sudo modprobe {modname}')
        else:
            log.error(f'Failed: sudo modprobe {modname}')
        installed_ko.append(modname)
    makeargs = []
    if args.jobs is not None:
        makeargs += ['-j', str(args.jobs)]
    makeargs = ' '+(' '.join(makeargs))
    BIN_DIR = config.paths.PATCHED_DIR / 'out' / 'linux.amd64' / 'release' / 'bin'
    log.info(str(BIN_DIR))
    INSTALL_SH = BIN_DIR / 'INSTALL.sh'
    installed_libs = []
    with os_utils.Chdir(str(BIN_DIR / 'src')):
        os_utils.cmd(['make', 'clean']+makeargs.strip().split(), echo=True, show_output=True)
        os_utils.cmd(['make']+makeargs.strip().split(), echo=True, show_output=True)
        os_utils.cmd(['sudo', 'make', 'install']+makeargs.strip().split(), echo=True, show_output=True)
        os_utils.cmd(['sudo', 'depmod', '-e', '-F', str(SYSTEM_MAP_LOC), os.uname().release], echo=True, show_output=True)
        with log.info('Installing kernel modules...'):
            install_kernel_module(config.strings.vboxdrv)
            install_kernel_module(config.strings.vboxnetadp)
            install_kernel_module(config.strings.vboxnetflt)
            #install_kernel_module(config.strings.vboxpci)
        INSTALL_DIR = Path('/usr/local/virtualbox')
        LIB_DIR = Path('/usr/lib')
        with log.info(f'Installing files to {INSTALL_DIR}...'):
            with click_spinner.spinner():
                INSTALL_DIR.mkdir(parents=True, exist_ok=True)
                os_utils.cmd(['sudo', 'cp', '-prf', str(BIN_DIR)+'/*', str(INSTALL_DIR)])
        with log.info(f'Installing files to {LIB_DIR}...'):
            for lib in tqdm.tqdm(LIB_DIR.glob('.so'), unit='file'):
                os_utils.cmd(['sudo', 'cp', '-prf', str(lib), str(INSTALL_DIR)])
                log.success(lib)
        with log.info('Installing symlinks...'):
            symlink(INSTALL_DIR / config.strings.VBoxManage, INSTALL_DIR / 'VBoxManage')
            symlink(INSTALL_DIR / config.strings.VBoxSVC,    INSTALL_DIR / 'VBoxSVC')
            symlink(INSTALL_DIR / config.strings.VirtualBox, INSTALL_DIR / 'VirtualBox')
    with KMODS_INSTALLED_FILE.open('w') as f:
        data = {
            'installed-kernel-modules': installed_ko,
            'libs': installed_libs,
        }
        yaml.dump(data, f)
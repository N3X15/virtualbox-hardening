from pathlib import Path
import random
import string
from typing import Any, Dict
import toml
from vboxharden.logging import ClickWrap
#from ruamel.yaml import YAML as Yaml

from vboxharden.paths import Paths
from vboxharden.strings import Strings

from vboxharden.utils import echo_vtable

#yaml = Yaml(typ='rt')
log = ClickWrap()

class Config:
    FILENAME: Path = Path('config.toml')
    def __init__(self) -> None:
        self.username: str = ''
        self.ip: str = '192.168.56.1'
        self.netmask: str = '255.255.255.0'
        self.network: str = '192.168.56.0'
        self.broadcast: str = '192.168.56.255'

        self.paths: Paths = Paths(self)
        self.strings: Strings = Strings(self)

    def configure(self, vbox_dir: Path) -> bool:
        self.strings.configure()
        self.paths.configure(vbox_dir)
        self.username = ''.join([random.choice(string.ascii_letters+string.digits) for _ in range(8)])

    def load(self, filename: Path) -> None:
        with filename.open('r') as f:
            self.deserialize(toml.load(f))

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.paths.deserialize(data['paths'])
        self.strings.deserialize(data['strings'])

        self.username = data['username']

        self.ip = data['network']['ip']
        self.netmask = data['network']['netmask']
        self.network = data['network']['network']
        self.broadcast = data['network']['broadcast']

    def save(self, filename: Path) -> None:
        with filename.open('w') as f:
            toml.dump(self.serialize(), f)

    def serialize(self) -> Dict[str, Any]:
        return {
            'username': self.username,
            'network': {
                'ip': self.ip,
                'netmask': self.netmask,
                'network': self.network,
                'broadcast': self.broadcast,
            },
            'paths': self.paths.serialize(),
            'strings': self.strings.serialize(),
        }

    def dump(self) -> None:
        log.raw('')
        with log.raw('Auth:'):
            echo_vtable({
                'Username': self.username
            })
        log.raw('')
        with log.raw('Networking:'):
            echo_vtable({
                'IP Address': self.ip,
                'Network Mask': self.netmask,
                'Network': self.network,
                'Broadcast': self.broadcast,
            })
        log.raw('')
        self.paths.dump()
        log.raw('')
        self.strings.dump()
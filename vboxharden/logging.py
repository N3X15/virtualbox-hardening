import click
class ClickWrap:
    PREFIX_INFO = click.style('[-]')
    PREFIX_SUCCESS = click.style('[+]', fg='green')
    PREFIX_WARNING = click.style('[*]', fg='yellow')
    PREFIX_ERROR = click.style('[!]', fg='red')

    LEVEL: int = 0
    CHARS: str = '  '

    def __init__(self) -> None:
        pass

    def getIndent(self) -> str:
        return ClickWrap.CHARS*ClickWrap.LEVEL
        
    def _fmt_str(self, prefix: str, msg: str) -> str:
        return f'{self.getIndent()}{prefix} {msg}'
    def info_str(self, msg: str) -> str:
        return self._fmt_str(self.PREFIX_INFO, msg)
    def success_str(self, msg: str) -> str:
        return self._fmt_str(self.PREFIX_SUCCESS, msg)
    def warning_str(self, msg: str) -> str:
        return self._fmt_str(self.PREFIX_WARNING, msg)
    def error_str(self, msg: str) -> str:
        return self._fmt_str(self.PREFIX_ERROR, msg)

    def _echo_str(self, prefix: str, msg: str) -> None:
        click.echo(self._fmt_str(prefix,msg))

    def info(self, msg: str) -> 'ClickWrap':
        self._echo_str(self.PREFIX_INFO, msg)
        return self
    def success(self, msg: str) -> 'ClickWrap':
        self._echo_str(self.PREFIX_SUCCESS, msg)
        return self
    def warning(self, msg: str) -> 'ClickWrap':
        self._echo_str(self.PREFIX_WARNING, msg)
        return self
    def error(self, msg: str) -> 'ClickWrap':
        self._echo_str(self.PREFIX_ERROR, msg)
        return self
    def raw(self, msg: str) -> 'ClickWrap':
        click.echo(f'{self.getIndent()}{msg}')
        return self

    def __enter__(self):
        ClickWrap.LEVEL += 1
        return self
    def __exit__(self, exc_type, exc_value, traceback):
        ClickWrap.LEVEL -= 1
        return self
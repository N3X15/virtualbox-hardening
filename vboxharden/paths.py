from pathlib import Path
import random
from typing import Any, Dict
import click, argparse
import string
from vboxharden.logging import ClickWrap

from vboxharden.utils import echo_vtable

log = ClickWrap()

ALPHA_TMPDIR = string.ascii_letters+string.digits+'_-.'

class Paths:
    def __init__(self, config: 'Config') -> None:
        self.config: 'Config' = config
        self.SOURCESDIR: Path = None
        #: where we find the kmk tools e.g. kmk_md5sum
        self.KMKTOOLSSUBDIR: Path = None
        #: log md5sum ops to this file
        self.MD5SUMOUT: Path = None
        self.VBOXMANAGE: Path = None
        self.TMPDIR: Path = None

        self.ORIG_DIR: Path = None
        self.PATCHED_DIR: Path = None

    def configure(self, source_dir: Path) -> None:
        self.SOURCESDIR = source_dir
        self.KMKTOOLSSUBDIR = self.SOURCESDIR / 'kBuild' / 'bin' / 'linux.amd64'
        self.MD5SUMOUT = self.SOURCESDIR / 'kmk_md5.out'
        self.VBOXMANAGE = self.SOURCESDIR / 'out' / 'linux.amd64' / 'release' / 'bin' / self.config.strings.VBoxManage
        self.reconfigureTmp()

    def reconfigureTmp(self) -> None:
        self.TMPDIR = Path('/tmp') / ('vboxharden-'+self.generateStringFrom(16, ALPHA_TMPDIR))
        self.ORIG_DIR = self.TMPDIR / 'orig'
        self.PATCHED_DIR = self.TMPDIR / 'patched'
        self.KMKTOOLSSUBDIR = self.PATCHED_DIR / 'kBuild' / 'bin' / 'linux.amd64'
        self.MD5SUMOUT = self.PATCHED_DIR / 'kmk_md5.out'
        self.VBOXMANAGE = self.PATCHED_DIR / 'out' / 'linux.amd64' / 'release' / 'bin' / self.config.strings.VBoxManage

    def generateStringFrom(self, strlen: int, alphabet: str) -> str:
        o = ''
        for _ in range(strlen):
            o += random.choice(alphabet)
        return o

    def serialize(self) -> Dict[str, Any]:
        return {
            'sources': str(self.SOURCESDIR),
            'kmk-tools': str(self.KMKTOOLSSUBDIR),
            'md5sums': str(self.MD5SUMOUT),
            'vboxmanage': str(self.VBOXMANAGE),
            'tmp': str(self.TMPDIR),
        }

    def deserialize(self, data: Dict[str, Any]) -> None:
        self.SOURCESDIR = Path(data['sources'])
        self.KMKTOOLSSUBDIR = Path(data['kmk-tools'])
        self.MD5SUMOUT = Path(data['md5sums'])
        self.VBOXMANAGE = Path(data['vboxmanage'])
        self.TMPDIR = Path(data['tmp'])
        self.ORIG_DIR = self.TMPDIR / 'orig'
        self.PATCHED_DIR = self.TMPDIR / 'patched'

    def dump(self) -> None:
        with log.raw('Paths:'):
            echo_vtable({
                'SOURCESDIR': self.SOURCESDIR,
                'KMKTOOLSSUBDIR': self.KMKTOOLSSUBDIR,
                'MD5SUMOUT': self.MD5SUMOUT,
                'VBOXMANAGE': self.VBOXMANAGE,
                'TMPDIR': self.TMPDIR,
            })
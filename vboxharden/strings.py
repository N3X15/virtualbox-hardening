import datetime
import os
import string
import re
import random
import shutil
import tqdm
from  functools import cached_property
from typing import Any, Dict, List, OrderedDict, Pattern
from pathlib import Path
from vboxharden.logging import ClickWrap
import collections

from vboxharden.utils import echo_vtable, is_binary, sizeof_fmt

log = ClickWrap()

ALPHANUM = string.ascii_letters+string.digits
RE_MATCH = re.compile(r'(QVBOXLAYOUT|VIRTUALBOX|VBOXMANAGE|VBOX|ORACLE|INNOTEK|PCI80EE)', re.IGNORECASE)
# sed -i 's/06\/23\/99/07\/24\/13/g' $SOURCESDIR/src/VXox/Devices/PC/BIOS/orgs.asm 
RE_BIOS_DATE = re.compile(r'(06\/23\/99)')
BIOS_DATE_START = datetime.date(1997, 1, 1)
BIOS_DATE_END = datetime.date(2000, 12, 30)
FORBIDDEN_EXTS = frozenset(['.exe', '.dll', '.so', '.a', '.o', '.bin', '.lib'])
# I tried regular old regex, but it resulted in compile failures, so I'm back to using string replace.
class Strings:
    def __init__(self, config: 'Config') -> None:
        self.config: 'Config' = config
        self.allMatches: OrderedDict[str, str] = collections.OrderedDict()
        self.biosDate: datetime.date = None
        self.configured: bool = False

    def generateBiosDate(self) -> datetime.date:
        return BIOS_DATE_START + datetime.timedelta(days=random.randrange((BIOS_DATE_END-BIOS_DATE_START).days))
    '''
    def getOrCreateMatch(self, orig: str) -> str:
        primekey: str = orig.upper()
        if primekey not in self.allMatches:
            if orig.upper().endswith('80EE'):
                nstr = self.generatePCI80StringFrom(orig)
            else:
                nstr = self.generateStringFrom(orig)
            self.allMatches[primekey] = nstr.upper()
        if primekey != orig:
            if orig not in self.allMatches:
                o = ''
                pk = self.allMatches[primekey]
                for i, c in enumerate(orig):
                    nc = pk[i]
                    o += (nc.upper() if c.isupper() else nc.lower()) if c.isalpha() else nc
                self.allMatches[orig] = o
        #log.info(f'{orig!r} -> {o}', fg='green')
        return self.allMatches[orig]

    def setMatch(self, orig: str, repl: str) -> None:
        primekey: str = orig.upper()
        self.allMatches[primekey] = repl.upper()
        key: str = orig
        self.allMatches[key] = repl

    @cached_property
    def VBoxManage(self) -> str:
        if 'VBoxManage' not in self.allMatches:
            vbox = self.getOrCreateMatch('VBox')
            self.setMatch('VBoxManage', vbox+self.generateStringFrom('Manage'))
        return self.getOrCreateMatch('VBoxManage')

    def generatePCI80StringFrom(self, orig: str) -> str:
        o = 'PCI80'
        for _ in range(2):
            o += random.choice(string.hexdigits)
        nc = ''
        nb = ''
        for i in range(len(orig)):
            nc = o[i]
            c = orig[i]
            nb += (nc.upper() if c.isupper() else nc.lower()) if c.isalpha() else nc
        return nb
    '''

    def addReplacement(self, orig: str, repl: str) -> None:
        self.allMatches[orig] = repl

    def generateStringFrom(self, orig: str, alphabet: str=ALPHANUM, first_char_alphabet: str=string.ascii_letters) -> str:
        o = ''
        for i, c in enumerate(orig):
            if i == 0:
                ab = first_char_alphabet
            else:
                ab = alphabet
            nc = random.choice(ab)
            o += (nc.upper() if c.isupper() else nc.lower()) if c.isalpha() else nc
        return o

    # def handle_match(self, match: re.Match) -> str:
    #     return self.getOrCreateMatch(match[1])

    @cached_property
    def VBoxManage(self) -> str:
        self._ensure_configured()
        return self.allMatches['VBoxManage']
    @cached_property
    def VBoxManage(self) -> str:
        self._ensure_configured()
        return self.allMatches['VBoxSVC']
    @cached_property
    def VBoxManage(self) -> str:
        self._ensure_configured()
        return self.allMatches['VirtualBox']

    @cached_property
    def vboxdrv(self) -> str:
        self._ensure_configured()
        return self.allMatches['vbox']+'drv'
    @cached_property
    def vboxpci(self) -> str:
        self._ensure_configured()
        return self.allMatches['vbox']+'pci'
    @cached_property
    def vboxnetflt(self) -> str:
        self._ensure_configured()
        return self.allMatches['vbox']+'netflt'
    @cached_property
    def vboxnetadp(self) -> str:
        self._ensure_configured()
        return self.allMatches['vbox']+'netadp'

    def _ensure_configured(self) -> None:
        if not self.configured:
            self.configure()
    

    def configure(self) -> bool:
        self.allMatches = {}

        Virtual: str = self.generateStringFrom('Virtual').capitalize()
        Box: str = self.generateStringFrom('Box').capitalize()
        V: str = Virtual[0]

        self.addReplacement('VirtualBox', Virtual+Box)
        self.addReplacement('virtualbox', (Virtual+Box).lower())
        self.addReplacement('VIRTUALBOX', (Virtual+Box).upper())
        self.addReplacement('virtualBox', Virtual.lower()+Box)

        self.addReplacement('VBoxManage', V+Box+self.generateStringFrom('Manage'))
        self.addReplacement('VBoxSVC', V+Box+self.generateStringFrom('SVC'))

        self.addReplacement('VBox', V+Box)
        self.addReplacement('vbox', (V+Box).lower())
        self.addReplacement('VBOX', (V+Box).upper())
        self.addReplacement('Vbox', V+(Box.lower()))


        Oracle = self.generateStringFrom('Oracle').capitalize()
        self.addReplacement('Oracle', Oracle)
        self.addReplacement('oracle', Oracle.lower())
        #self.addReplacement('ORACLE', Oracle.upper())

        Inno = self.generateStringFrom('Inno').capitalize()
        Tek = self.generateStringFrom('Tek').capitalize()
        self.addReplacement('innotek', (Inno+Tek).lower())
        self.addReplacement('InnoTek', Inno+Tek)

        EE = ''.join([random.choice(string.hexdigits) for _ in range(2)])
        self.addReplacement('80EE', '80'+EE.upper())
        self.addReplacement('80ee', '80'+EE.lower())

        self.addReplacement('Q'+V+Box+'Layout', 'QVBoxLayout')
        
        self.configured = True

    def getOutPath(self, inpath: Path) -> Path:
        sp = str(inpath.relative_to(self.config.paths.ORIG_DIR))
        #csp = RE_MATCH.sub(self.handle_match, sp)
        csp = sp+''
        for pattern, repl in self.allMatches.items():
            csp = csp.replace(pattern, repl)
        return self.config.paths.PATCHED_DIR / csp

    def patch(self) -> bool:
        path: Path
        np=0
        ni=0
        with log.info('Patching...'):
            pb = tqdm.tqdm(list(self.config.paths.ORIG_DIR.rglob('*')), desc='Patching', unit='file')
            for path in pb:
                if not path.is_file():
                    continue
                outpath = self.getOutPath(path)
                outpath.parent.mkdir(parents=True, exist_ok=True)
                last_suffix = path.suffix
                if len(path.suffixes) > 1:
                    last_suffix = path.suffixes[-1]
                try:
                    if last_suffix not in FORBIDDEN_EXTS and not is_binary(path):
                        contents = path.read_text()
                        #contents = RE_MATCH.sub(self.handle_match, contents)
                        for pattern, repl in self.allMatches.items():
                            contents = contents.replace(pattern, repl)
                        outpath.write_text(contents)
                        #click.secho(f'[+] cp {path} -> {outpath} ({sizeof_fmt(os.path.getsize(path))})', fg='green')
                        pb.write(log.success_str(f'cp {path} -> {outpath} ({sizeof_fmt(os.path.getsize(path))})'))
                        np += 1
                    else:
                        #click.secho(f'[-] cp {path} -> {outpath} ({sizeof_fmt(os.path.getsize(path))})')
                        pb.write(log.info_str(f'cp {path} -> {outpath} ({sizeof_fmt(os.path.getsize(path))})'))
                        shutil.copy2(path, outpath)
                        ni += 1
                except Exception as e:
                    pb.close()
                    print(repr(e))
                    print(str(path))
                    return False
            
            pb.write(log.success_str(f'Patched {self.patchBiosDate()} (BIOS date)'))
        log.success(f'Done! {np:,} files patched, {ni:,} files copied')
        return True

    def patchBiosDate(self) -> Path:
        asmInPath = self.config.paths.ORIG_DIR / 'src' / 'VBox' / 'Devices' / 'PC' / 'BIOS' / 'orgs.asm'
        asmOutPath = self.getOutPath(asmInPath)
        asm = asmInPath.read_text()
        asm = RE_BIOS_DATE.sub(asm, self.biosDate.strftime('%m/%d/%y'))
        asmOutPath.write_text(asm)
        return asmOutPath

    def postConfigurePatch(self) -> bool:
        return


    def serialize(self) -> Dict[str, Any]:
        return {
            'bios-date': self.biosDate.isoformat(),
            'strings': self.allMatches
        }

    def deserialize(self, data: Dict[str, Any]) -> None:
        if 'bios-date' in data:
            self.biosDate = datetime.date.fromisoformat(data['bios-date'])
        else:
            self.biosDate = self.generateBiosDate()
        if 'strings' in data:
            self.allMatches = data['strings']
        else:
            self.allMatches = data

        self.configured = True

    def dump(self) -> None:
        log.raw(f'BIOS Date: {self.biosDate}')
        with log.raw('Strings:'):
            echo_vtable(self.allMatches)
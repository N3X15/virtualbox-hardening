from pathlib import Path
from typing import Any, Dict

from vboxharden.logging import ClickWrap

log = ClickWrap()
def sizeof_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"

class NonExistentDirectoryError(Exception):
    pass

def existingDirPath(inp: str) -> Path:
    p = Path(inp)
    if not p.is_dir():
        raise NonExistentDirectoryError()
    return p

class NonExistentFileError(Exception):
    pass

def existingFilePath(inp: str) -> Path:
    p = Path(inp)
    if not p.is_file():
        raise NonExistentFileError()
    return p

def echo_vtable(data: Dict[str, Any]) -> None:
    max_keylen = max([len(k) for k in data.keys()])+1
    for k, v in data.items():
        log.raw(f'{k:.<{max_keylen}}: {v}')

def is_binary(path: Path) -> bool:
    try:
        path.read_text()
    except:
        return True
    return False
